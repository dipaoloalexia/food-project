<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="gallery/normalize.css" />
<link rel="stylesheet" href="gallery/gallery.css" />
<title>DipaDegui cooking list</title>
</head>
<body>
	<h1>${ recipe.title }</h1>	
	<div>
		<img alt="" src="${ recipe.thumbnail }">
	</div>
	<p> ${ recipe.description } </p>
</body>
</html>