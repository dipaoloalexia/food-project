<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>List of recipes</title>
<link rel="stylesheet" href="gallery/normalize.css" />
<link rel="stylesheet" href="gallery/gallery.css" />
</head>

<body>
	<h1>Cooking recipes</h1>
	<div class="gallery">
		
		
		<c:forEach var="item" items="${items}" varStatus="status">
			<div class="gallery__item">
				<a href="DisplayDetailServlet?index=${ status.index }">
				<img class="gallery__item__image"
					src="${ item.thumbnail }" alt="" />
					<p class="gallery__item__title">${ item.title }</p> 
				</a>
			</div>
		</c:forEach>
	</div>

</body>
</html>