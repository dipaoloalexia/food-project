package com.dipadegui.trainings.entity;

public class Recipe {

	private String title;
	private String thumbnail;
	private String description;
	
	public Recipe(String title, String thumbnail, String description) {
		super();
		this.title = title;
		this.thumbnail = thumbnail;
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public String getDescription() {
		return description;
	}

}
