package com.dipadegui.trainings.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.dipadegui.trainings.entity.Recipe;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Servlet implementation class ListItemServlet
 */
@WebServlet("/ListItemServlet")
public class ListItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(DisplayDetailServlet.class);

	@Resource(lookup = "java:jboss/DataSources/RecipeDS")
	private transient DataSource recipeDS;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Recipe> items = new ArrayList<>();
		Recipe recipe = null;

		String sql = "SELECT * FROM recipe.recipe";
		try (Connection connection = recipeDS.getConnection();
				PreparedStatement prepareStatement = connection.prepareStatement(sql);) {

			// 3) executer la requete
			ResultSet resultSet = prepareStatement.executeQuery();

			// 4) traiter les résultats
			while (resultSet.next()) {
				recipe = new Recipe(resultSet.getString("name"), resultSet.getString("URL"),
						resultSet.getString("description"));
				items.add(recipe);
			}
		} catch (SQLException | NullPointerException e) {
			logger.catching(e);
			response.sendError(500, "Error communicating with the database");
		}

		try {
			request.setAttribute("items", items);
			request.getRequestDispatcher("/WEB-INF/gallery.jsp").forward(request, response);
		} catch (ServletException | IOException | IllegalStateException e) {
			logger.catching(e);
			response.sendError(500, "Error in the request dispatcher ");
		}
	}
}
