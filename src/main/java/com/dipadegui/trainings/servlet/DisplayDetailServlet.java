package com.dipadegui.trainings.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dipadegui.trainings.entity.Recipe;

/**
 * Servlet implementation class DisplayDetailServlet
 */
@WebServlet("/DisplayDetailServlet")
public class DisplayDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(DisplayDetailServlet.class);

	@Resource(lookup = "java:jboss/DataSources/RecipeDS")
	private transient DataSource recipeDS;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String index = request.getParameter("index");
		Recipe recipe = null;
		Integer pos = null;

		try {
			pos = Integer.valueOf(index) + 1;
		} catch (NumberFormatException e) {
			logger.catching(e);
			response.sendError(500, "Error with the index position in the database");
		}

		String sql = "SELECT * FROM recipe.recipe WHERE id=?";
		try (Connection connection = recipeDS.getConnection();
				PreparedStatement prepareStatement = connection.prepareStatement(sql);) {
			prepareStatement.setObject(1, pos);

			// 3) executer la requete
			ResultSet resultSet = prepareStatement.executeQuery();

			// 4) traiter les résultats
			while (resultSet.next()) {
				recipe = new Recipe(resultSet.getString("name"), resultSet.getString("URL"),
						resultSet.getString("description"));
			}

		} catch (SQLException | NullPointerException e) {
			logger.catching(e);
			response.sendError(500, "Error communicating with the database");
		}

		try {
			request.setAttribute("recipe", recipe);
			request.getRequestDispatcher("/WEB-INF/detail.jsp").forward(request, response);
		} catch (ServletException | IOException | IllegalStateException e) {
			logger.catching(e);
			response.sendError(500, "Error in the request dispatcher ");
		}
	}
}
